[![pipeline status](https://gitlab.com/bestminr/user-source/badges/master/pipeline.svg)](https://gitlab.com/bestminr/user-source/commits/master)
[![coverage report](https://gitlab.com/bestminr/user-source/badges/master/coverage.svg)](https://gitlab.com/bestminr/user-source/commits/master)

Inspect user source by referer string.
