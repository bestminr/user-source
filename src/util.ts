export function forIn<T, K extends keyof T>(
  obj: T,
  callback: (value: T[K], key: K) => false | void
) {
  for (const key in obj) {
    if (!obj.hasOwnProperty(key)) {
      continue;
    }
    const k = key as K;
    if (callback(obj[k], k) === false) {
      return;
    }
  }
}
