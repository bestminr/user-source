import { forIn } from './util';

const BAIDU_PROMO_PATTERN = /baidu\.php/;
const BAIDU_SEARCH_PATTERN = /baidu\.com\/link/;
const BAIDU_KEYWORD_PATTERN = /word=([=\w\d%]+)\&/;
const SOGOU_PROMO_PATTERN = /sogou\.com\/bill/;
const SOGOU_SEARCH_PATTERN = /sogou\.com\/link/;
const SOGOU_QUERY_PATTERN = /query=([=\w\d%]+)/;
const SOGOU_KEYWORD_PATTERN = /query=([=\w\d%]+)/;
const SO_PROMO_PATTERN = /\w+.so.com\/s/;
const SO_SEARCH_PATTERN = /www.so.com\/link/;
const SO_QUERY_PATTERN = /q=([=\w\d%]+)/;
const SO_KEYWORD_PATTERN = /q=([=\w\d%]+)\&?/;

type InspectResult = {
  source?: string;
  keyword?: string;
  query?: string;
};

type RuleSEARCH_PATTERN_MAP = {
  [key: string]: RegExp[];
};

const PROMO_PATTERN_MAP: RuleSEARCH_PATTERN_MAP = {
  '360-tuiguang': [SO_PROMO_PATTERN, SO_KEYWORD_PATTERN],
  'baidu-tuiguang': [BAIDU_PROMO_PATTERN, BAIDU_KEYWORD_PATTERN],
  'sogou-tuiguang': [SOGOU_PROMO_PATTERN, SOGOU_KEYWORD_PATTERN],
};

const SEARCH_PATTERN_MAP: RuleSEARCH_PATTERN_MAP = {
  '360-search': [SO_SEARCH_PATTERN, SO_QUERY_PATTERN],
  'baidu-search': [BAIDU_SEARCH_PATTERN],
  'sogou-search': [SOGOU_SEARCH_PATTERN, SOGOU_QUERY_PATTERN],
};

export function inspectSource(rf: string | null): InspectResult {
  if (!rf) {
    return {};
  }
  const obj: InspectResult = {};

  // 推广
  let isPromo = false;

  forIn(PROMO_PATTERN_MAP, (v, k: string) => {
    const [promoPattern, keywordPattern] = v;
    if (promoPattern.test(rf)) {
      const kMatches = keywordPattern.exec(rf);
      isPromo = true;
      obj.source = k;
      if (kMatches) {
        obj.keyword = decodeURIComponent(kMatches[1]);
      }
      return false;
    }
  });

  if (!isPromo) {
    forIn(SEARCH_PATTERN_MAP, (v, k: string) => {
      const [sp, queryPattern] = SEARCH_PATTERN_MAP[k];
      if (sp.test(rf)) {
        obj.source = k;
        if (queryPattern) {
          const qMatches = queryPattern.exec(rf);
          if (qMatches) {
            obj.query = decodeURIComponent(qMatches[1]);
          }
        }
      }
    });
  }
  return obj;
}
