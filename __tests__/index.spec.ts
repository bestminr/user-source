import { inspectSource } from '../src/index';

const assert = (a: any, b: any) => {
  expect(a).toEqual(b);
};

describe('statistics module', () =>
  describe('inspectSource', function() {
    it('detects sogou search', function() {
      const rf =
        'https://www.sogou.com/link?url=LeoKdSZoUyAYaSMaS2hSF7DBs2fYWW79F9l8XH3KswaKNQ2XXgS40Nsqj0xL2El-0CMIyv9G_L63fU-WItz07o-wEG3JwuyuNaEZ_xhJZtTseKztzuW6_yLo59mCR3glmQIspH_9SBckDKlCaA-AluoN4yDCQ61T7axdbtPtQSHhMuWJMChyFg..';
      const obj = inspectSource(rf);
      assert(obj.source, 'sogou-search');
    });

    it('detects sogou tuiguang and keyword', function() {
      const rf =
        'http://www.sogou.com/bill_cpc?v=1&p=WJed6FC711lUeTgizmIjAccVuzqaIliSc0sEbVdSxxVHjPwkewIyi5k07Na9AW16avZ9AYQV86hqYwjOYuGxq9pVeGj4eGxjYyYP7FlZDAceIQalFTEs@K@sQKfNXbMxTfg85hDNmTBA@pxrbLVeXVozTdZNwLgEdHmszK5V8$DsLF5T58daXTsdM5sqhLMWEmMOQ0moN@IfEmQF0eMWsE$V7O4yRlBT1wdU3ciEhMqhfRIMf2Ie3KwERz7UQ14Rj6IL1ju4F6pHLBqxH1RC9Sy@f9Bmx1vEYu09YxiSbQAYcgPkdA$NFI1Z6enF$gq3n2UYri7f1Z8Vk4u09szr5r1cgag5nMHrqd0BLx$x4CU3O3T$63oU67u9V3uflo6rysw@lZcrmOtSfxVwHy$JCABI7piCXU5c9T5jj8qmS3cqLATeFjC1sBEsA4bOsDkGFmIm0oJZmAmG3QAju0lOmlOZkIel4feZmogm5HIdsTY5iYAAV5TW2XrcbK@FsJhFUwkS6WtOCG38e2xCCzvtjTDoK3kLSPK6JvXFp@ongAv9$SIXRUK0I76HFe9g7V5R1VFqY53GOprppjXh9vXqbM56Cc2jpTAp9OLPAIAP8bd36CJW9it7NNujDnxmyV0@ej4sVwSDqGr96Ns4QzUwwtdJuv0hVBeuPG6aU7zCES05zwSr3m4hEGFZxNW1HUQbOdadd$vb6TamSOZ5m63tPl1TDsxeUOSqe0v29yrs3xYVaJhsVtpXMyLX7d4a&q=WJ8a3j6I3abvChDsadtr86@5adqa87@qBZllB@ow3l==&query=%E6%A2%85%E5%AD%90%E9%85%92';
      const obj = inspectSource(rf);
      assert(obj.source, 'sogou-tuiguang');
      assert(obj.keyword, '梅子酒');
    });

    it('detects baidu search', function() {
      const rf =
        'https://www.baidu.com/link?url=YX2vVS7jhkvLBqa2T6EskQQ3SyvZX1bu6LPE1oARLjHn6AP_Y3f5535VxAVDVzL2Ex84Y2OQe4xbkCS86IpGOddVtkVtZBvJVL30QEADBYkcFPoE7TgfAWMwwf52L8OI&wd=&eqid=80fac67b000173b4000000025bfe1624';
      const obj = inspectSource(rf);
      assert(obj.source, 'baidu-search');
    });

    it('detects baidu tuiguang and keyword', function() {
      const rf =
        'http://www.baidu.com/baidu.php?url=0f0000jeTx0mG_-ZTZ2jRHFW1DyEBvkc9MU4rXn_emEdL-czMWNfCpUJvdNx7z5QyM6YB1CPdWFn8Buev5mJ1-2-u67JwZhfqdyJP4cbX6DxGfeH0hrU6LLtvjYriObWuHfiuMd2Fd8ctjBt9x6RkgURZWa5XyaXOnar-7Rld9w74c2hGsK78uX-wJhP7p_1XIoB2nXzg7SIvwzo7f.7b_NR2Ar5Od66sw5I7M9CE9exQQTBaPrMLek8sHfGmEukmntpyuCp88aFeCEETkblTMWYS5ZIblkECFSq17erQKA-hpAo-zyUV-MuvUVLH3qpjh2J7jZ93SLjbS8ZFqubSyy4AZFqmY_1fdmX5deRlrKYdvZWt_5MY3IOW9q8ej4qrZxtrZvOj9tSMjeThZu8sSXejex-9Lvmx5x9vU_rOF9zI5pMwsrh8kmX5zzyUV-MuvUVLH33TMHnM-89k3X5zzyuPPM4tELSSojkSyZWt_rHGCHnlqU3XyPvap7rZWtJeQr1jblpamlAZWtJtrHGH7j93OQKfde72s1f_U85u_lN0.U1YY0ZDqsexgYeeA0ZKGm1Yk0ZfqzPykE_rLdqrSk6KGUHYznjf0u1dBugK1nfKdpHdBmy-bIfKspyfqnfKWpyfqn16z0AdY5HDsnHPxnH0krNtknjDLg1nznW9xn1msnfKopHYs0ZFY5HDvnfK-pyfqnWmdnWPxnHfzP7tznHDzn-tzPWndn7tzP1RsrfKBpHYYnjFxnW0Yg1DdPfKVm1YkPWmsP101P19xnH0snNt3njf4P1fdnjwxnH0zg100TgKGujYs0Z7Wpyfqn0KzuLw9u1Ys0A7B5HKxn0K-ThTqn0KsTjYs0A4vTjYsQW0snj0snj0s0AdYTjYs0AwbUL0qn0KzpWYs0Aw-IWdsmsKhIjYs0ZKC5H00ULnqn0KBI1Ykn0K8IjYs0ZPl5fKYIgnqnHnknWfzrHcvrjTsnWDkPW6LnH60ThNkIjYkPHf1n1T3Pjm3n1bs0ZPGujY3nAu9m1mLmW0snjDLnvcY0AP1UHY4fW7Knbf1f1bvfHRvwjuA0A7W5HD0TA3qn0KkUgfqn0KkUgnqn0KlIjYs0AdWgvuzUvYqn7tsg1Kxn7ts0Aw9UMNBuNqsUA78pyw15HKxn7tsg1n1nj0srj7xn0Ksmgwxuhk9u1Ys0AwWpyfqn0K-IA-b5iYk0A71TAPW5H00IgKGUhPW5H00Tydh5HDv0AuWIgfqn0KhXh6qn0Khmgfqn0KlTAkdT1Ys0A7buhk9u1Yk0Akhm1Ys0APzm1YvPjbdrf&word=%E6%A2%85%E5%AD%90%E9%85%92&ck=3695.32.9999.444.637.307.143.785&shh=www.baidu.com&sht=baidu&us=3.0.1.0.4.1546.0';
      const obj = inspectSource(rf);
      assert(obj.source, 'baidu-tuiguang');
      assert(obj.keyword, '梅子酒');
    });

    it('detects 360 tuiguang and keyword', function() {
      const rf =
        'https://www.so.com/s?ie=utf-8&fr=so.com&src=home-sug-store&q=%E6%A2%85%E5%AD%90%E9%85%92';
      const obj = inspectSource(rf);
      assert(obj.source, '360-tuiguang');
      assert(obj.keyword, '梅子酒');
    });

    it('detects 360 search', function() {
      const rf =
        'http://www.so.com/link?m=aafaEQ3A%2BvwD5RqbsSXOKzEOmEMFz418ncvqYdh4OdPfjDkU0dRrGLLX5ABREVBAH7lf1ezyRsPzvHoZm%2FlM1h0A0VzLig5%2FJO9oMseVf4aBxL0lI9b4%2FOIFhqZJa7ZOT';
      const obj = inspectSource(rf);
      assert(obj.source, '360-search');
    });
  }));
